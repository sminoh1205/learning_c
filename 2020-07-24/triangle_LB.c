#include <stdio.h>
#include <stdbool.h>



void triangleLB(int n)
{
    for(int i = 0; i < n; i ++)
    {
        for(int j = 0; j <= i; j++)
        {
            printf("*");
        }
        printf("\n");
    }
}

void triangleLU(int n)
{
    for(int i = n; i > 0; i--)
    {
        for(int j = 0; j < i; j++)
        {
            printf("*");
        }
        printf("\n");
    }
}

void triangleRU(int n)
{
    for(int i = n; i > 0; i--)
    {
        bool space = false;
        for(int j = 0; j < i; j++)
        {
            if(n-i > 0 && space == false)
            {
                for(int z = 0; z < n-i; z++)
                {
                    printf(" ");
                }
                space = true;
            }
            printf("*");
        }
        printf("\n");
    }
}

void triangleRB(int n)
{
    for(int i = 0; i < n; i ++)
    {
        for(int j = n-1; j > i; j--)
        {
            printf(" ");
        }
        for(int z = 0; z <= i; z++)
        {
            printf("*");
        }
        printf("\n");
    }
}
void main(void)
{
    // triangleLB(5);
    // triangleLU(5);
    // triangleRU(5);
    triangleRB(5);
}