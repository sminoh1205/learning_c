#include <stdio.h>

int main(void)
{
    // h = height
    // w = width
    int h,w;

    do
    {
        printf("height : "); scanf("%d", &h);
        printf("width : "); scanf("%d", &w);
    } while (h < 0 && w < 0);
    
    for(int i = 0; i < h; i++)
    {
        for(int j = 0; j < w; j++)
        {
            printf("*");
        }
        printf("\n");
    }
    return 0;
}