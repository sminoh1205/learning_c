#include <stdio.h>

int main(void)
{
    int n;
    do
    {
        puts("enter size of a square : ");
        scanf("%d",&n);
    } while (n < 0);
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < n; j++)
        {
            printf("*");
        }
        printf("\n");
    }
    
}