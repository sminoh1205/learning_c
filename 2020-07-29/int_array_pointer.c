#include <stdio.h>
#include <stdlib.h>

void main(void)
{
    int i;
    int *a; //pointer
    int na;
    printf("array size :"); scanf("%d", &na);
    a = calloc(na, sizeof(int)); //point to a memory location with size of [4 bytes * array size]

    if(a == NULL)
    {
        puts("memory allocation failed.");
    }
    else
    {
        printf("type %d many int.\n", na);
        for(i = 0; i < na; i++)
        {
            printf("a[%d] : ", i); scanf("%d", &a[i]);
        }
        printf("printing array.");
        for(i = 0; i < na; i++)
        {
            printf("a[%d] = %d\n", i, a[i]);
        }
        free(a);
    }    
}