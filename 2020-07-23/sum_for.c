#include <stdio.h>

int main(void)
{
    int i, n;
    int sum;
    puts("Find sum from 1 to n.");
    printf("n : ");
    scanf("%d", &n);
    sum = 0;
    for(i = 1; i <= n; i ++)
    {
        sum += i;
    }
    printf("Total is %d.\n", sum);

}