#include <stdio.h>

int g_sum(int a, int b)
{
    int length;
    if(a < b)
    {
        length = b - a + 1;
    }
    else
    {
        length = a - b + 1;
    }
    return ((a + b) * length) / 2;
}

int main(void)
{
    int a,b;
    puts("Enter two numbers you want to find total sum in between.");
    printf("a : "); scanf("%d", &a);
    printf("b : "); scanf("%d", &b);
    printf("Total sum of numbers between %d and %d is %d.\n", a, b, g_sum(a,b));
}