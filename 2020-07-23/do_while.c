#include <stdio.h>

int main(void)
{
    int i, n;
    int sum;
    puts("Find total sum from 1 to n.");

    do
    {
        printf("n : "); scanf("%d", &n);
    } while (n < 1);
    sum = 0;
    for(i = 1; i <= n; i ++)
    {
        sum += i;
    }
    printf("Total sum from 1 to %d is %d.\n", n, sum);
}