#include <stdio.h>

int min3(int a, int b, int c)
{
    int min = a;
    if(b < min) min = b;
    if(c < min) min = c;
    return min;
}

int main(void)
{
    printf("min3(%d,%d,%d) = %d\n", 1,2,3, min3(1,2,3));
    int d, e, f;
    int min_u;
    printf("Find min3.\n");
    printf("Type 1st number : "); scanf("%d", & d);
    printf("Type 2nd number : "); scanf("%d", & e);
    printf("Type 3rd number : "); scanf("%d", & f);
    min_u = d;
    if(e < min_u) min_u = e;
    if(f < min_u) min_u = f;

    printf("Min num is %d.\n", min_u);
    return 0;
}