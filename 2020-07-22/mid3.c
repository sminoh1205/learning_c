#include <stdio.h>

int mid3(int a, int b, int c)
{
    if((b >= a && a >= c) || (c >= a && a >= b)) return a;
    else if((a >= b && b >= c) || (c >= b && b >= a)) return b;
    else return c;
}

int main(void)
{
    int a,b,c;
    printf("a : "); scanf("%d", & a);
    printf("b : "); scanf("%d", & b);
    printf("c : "); scanf("%d", & c);
    printf("middle number is %d\n",mid3(a,b,c));
}