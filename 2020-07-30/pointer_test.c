#include <stdio.h>
#include <stdlib.h>

void main(void)
{
    int *p;
    int *q;
    int n = 100;
    printf("p's address: %d\n", p); // p's address
    p = &n; // now points to n
    printf("p pointing n's address: %d\n", p); // n's address
    printf("p pointing n: %d\n", *p); // n
    q = p; // q points to n as well
    printf("q pointing n's address: %d\n", q);
    printf("q pointing n: %d\n", *q);
    *p = 2; // set n to 2
    printf("p: %d\n", *p);
    *p = *p + *p; // set n to 4
    printf("p: %d\n", *p);
    printf("n: %d\n", n);

    (*p)++; // increment the value pointed to by p. so increment n.
    printf("n: %d\n", n);
    *p++; // increments p itself.
    printf("p: %d\n", *p);
    printf("n: %d\n", n);
}