#include <stdio.h>

int card_convr(unsigned x, int n, char d[])
{
    char dchar[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int digits = 0;
    if(x==0)
    {
        d[digits++] = dchar[0];
        printf("%d",digits);
    }
    else
    {
        while(x)
        {
            d[digits++] = dchar[x % n];
            x /= n;
        }
    }
    return digits;
}

int main(void)
{
    int i;
    unsigned no;
    int cd;
    int dno;
    char cno[512];
    int retry;
    puts("10 진수를 기수 변환합니다");
    do
    {
        printf("변환하는 음이 아닌 정수 : ");scanf("%u", &no);
        do
        {
            printf("어떤 진수로 변환할까요?(2-36) : ");
            scanf("%d", &cd);
        }while (cd < 2 || cd > 36);
    dno = card_convr(no, cd, cno);
    printf("%d 진수로는 ", cd);
    for(i = dno-1; i>=0; i--)
    {
        printf("%c", cno[i]);
    }
    printf(" 입니다.\n");
    printf("one more time? (1 --- yes, 0 --- no) : ");
    scanf("%d", &retry);
    } while(retry == 1);
    return 0;   
}