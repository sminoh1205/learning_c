#include <stdio.h>
#include <stdlib.h>

void main(void)
{
    int * x;
    x = calloc(1, sizeof(int));
    if(x == NULL)
    {
        puts("fail to allocate memory.");
    }
    else{
        *x = 57;
        printf("*x = %d\n", *x);
        free(x);
    }
}