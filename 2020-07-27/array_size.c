#include <stdio.h>

void main(void)
{
    int i;
    int a[5] = {1,2,3,4,5};
    /* One integer is size of 4 bytes.
     * a is total of 20 bytes.
     * To get the length [total size / one integer size] which is 20 / 4 so 5. */
    int length = sizeof(a) / sizeof(a[0]);
    printf("length of a is %d.\n", length);

    for(i = 0; i < length; i++)
        printf("a[%d] = %d\n", i, a[i]);

}